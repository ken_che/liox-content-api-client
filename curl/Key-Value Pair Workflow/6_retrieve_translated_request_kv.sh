curl -X GET --header 'Accept: application/json' --header 'Authorization: %%%USER_TOKEN_STRING%%%' 'https://content-api.lionbridge.com/v1/jobs/e093fbc0-ddbc-4eef-9469-aad71f23ab38/requests/e8e7dc48-678a-4eec-80a4-5955f222a82e/retrieve'

# [
  # {
    # "requestId": "e8e7dc48-678a-4eec-80a4-5955f222a82e",
    # "sourceNativeId": "introduction page/english",
    # "sourceNativeLanguageCode": "en",
    # "targetNativeId": "introduction page/spanish",
    # "targetNativeLanguageCode": "es",
    # "structuredContent": [
      # {
        # "key": "p1",
        # "value": "Xxxxx xxxxx xxxxx xxx xxxx, xxxxxxxxxxxx xxxxxxxxxx xxxx. Xxxxxx xxxxxxx xxxxxx xxxx xxxxx. Xxxxxx xxxxx. Xxx xxxxxx xxxxxxx xxxxxxxxx xx xxxxxx xxx xxxxxxxxxx xxxxxx, xxxxxxxx xxxxxxxxx xxx."
      # },
      # {
        # "key": "p2",
        # "value": "Xxxxx xxxx xxxxx, xxxxxxxxx xxx, xxxxxxxxxxxx xx, xxxxxxx xxxx, xxx. Xxxxx xxxxxxxxx xxxxx xxxx xxxx. Xxxxx xxxx xxxxx, xxxxxxxxx xxx, xxxxxxx xxx, xxxxxxxxx xxxx, xxxx."
      # },
      # {
        # "key": "p3",
        # "value": "Xx xxxx xxxxx, xxxxxxx xx, xxxxxxxxx x, xxxxxxxxx xxxxx, xxxxx. Xxxxxx xxxxxx xxxxx xx xxxx xxxxxx xxxxxxx. Xxxxxxx xxxxxxxxx. Xxxx xxxxxxx. Xxxxxxx xxxxxxxxx xxxxxx xxxx. Xxxxxx xxxxxxxxx xxxxxxxx xxxxxx. Xxxxxx xxx xxxxxx, xxxxxxxxx xx, xxxxxxxxx xxxxx, xxxxxxxx xx, xxxx. Xxxxxxx xxxxx xxxx, xxxxxxx xx, xxxxxxx xxxx, xxxxxxx x,"
      # }
    # ],
    # "unstructuredContent": []
  # }
# ]